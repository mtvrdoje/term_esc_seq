# term_esc_seq

## Description
terminal escape sequences

# Notes    
    -------- GENERAL --------
        HELPFUL:
            You can run cat and press S-Left to see what is being sent
            
        REFERENCES:
            most helpful:
                http://www.leonerd.org.uk/hacks/fixterms/
            additional:
                https://bestasciitable.com/
                https://docs.kde.org/trunk5/en/konsole/konsole/konsole.pdf
                https://tldp.org/HOWTO/Text-Terminal-HOWTO-16.html
                https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
                https://tldp.org/HOWTO/pdf/Keyboard-and-Console-HOWTO.pdf
                vim
                    https://superuser.com/a/705494
                    https://vim.fandom.com/wiki/Mapping_fast_keycodes_in_terminal_Vim

            extras:
                https://superuser.com/a/1469994
                    good explanation how keyboard and esc seqs work
                https://unix.stackexchange.com/a/631242
                    good explanation os esc seq setup
                        one mistake though, ctrl+shift modifier code is 6, not 5
                https://askubuntu.com/a/434250
                    good explanation of how ctrl+letter combinations work in terminal/bash
                https://en.wikipedia.org/wiki/ANSI_escape_code
                https://en.wikipedia.org/wiki/VT100
                https://vt100.net/docs/vt102-ug/chapter5.html#S5.5
                https://www.vt100.net/docs/vt100-ug/chapter3.html#S3.3.3
                https://wiki.bash-hackers.org/scripting/terminalcodes
                    interesting uses of esc seqs - coloring and stuff
                https://www.gnu.org/software/screen/manual/html_node/Control-Sequences.html
                    screen utility esc seq codes
                https://www.xfree86.org/current/ctlseqs.html
                    xterm esc seq codes
                
    -------- COMMENTS -------
    -HOME, END, ARROWS 
        - Main reference:
            http://www.leonerd.org.uk/hacks/fixterms/

            - additions to the ref:        
                - identifiers(_LETTER_):
                    A: UP
                    B: DOWN
                    C: RIGHT
                    D: LEFT
                    F: END
                    H: HOME
                - without modifiers 
                    normally, esc seq would be: \E[1;_LETTER_*
                    but, 1; should be ommited so this becomes \E[_LETTER_
                    but but, this wont be catched by eg vim by default
                    more general format seems to be \EO_LETTER_, so that should be used
                        \E[_LETTER_ == \EO_LETTER_

# TODO
    - better understanding how keys/key seqs are generated - from keyboard hardvare to OS to terminal emulator to final app
        start ref: https://superuser.com/a/1469994





# Editing this README
When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.
